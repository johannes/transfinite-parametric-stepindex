From Coq Require Import ssreflect.
From stdpp Require Import strings gmap.
From transfinite.base_logic.lib Require Import ghost_map ghost_var gen_heap.
From iris.algebra Require Import gset dfrac gmap_view.
From iris.proofmode Require Import proofmode.

Class DFractional `{SI: indexT} {Σ : gFunctors} (Φ : dfrac → iProp Σ) := {
  compat_valid d : Φ d ⊢ ✓ d;
  compat_op d1 d2 : Φ d1 ∗ Φ d2 ⊣⊢ Φ (d1 ⋅ d2);
  compat_pcore d :> CoreId d → Persistent (Φ d);
  compat_update d1 d2 : d1 ~~> d2 → Φ d1 ⊢ |==> Φ d2
}.

Section DFractionalFacts.
  Context `{SI: indexT} {Σ : gFunctors} {Φ : dfrac → iProp Σ}.
  Context {HD : DFractional Φ}.
  Lemma compat_valid_pure d : Φ d ⊢ ⌜✓ d⌝.
  Proof using HD. iIntros "H"; iPoseProof (compat_valid with "H") as "%HH"; done. Qed.
  Lemma compat_valid_update d dc : pcore d = Some dc → Φ d ⊢ □ Φ dc.
  Proof using HD.
    iIntros (Hcore) "H".
    assert (dc ⋅ d = d) as <- by apply (cmra_pcore_l d dc Hcore).
    assert (CoreId dc) as Hid by apply (cmra_pcore_idemp d dc Hcore).
    iPoseProof (compat_op with "H") as "[#$ _]".
  Qed.
  Lemma compat_op_L d1 d2 : Φ d1 ∗ Φ d2 ⊢ Φ (d1 ⋅ d2).
  Proof using HD.
    iIntros "H". by iApply compat_op.
  Qed.
  Lemma compat_op_R d1 d2 : Φ (d1 ⋅ d2) ⊢ Φ d1 ∗ Φ d2.
  Proof using HD.
    iIntros "H". by iApply compat_op.
  Qed.
  Lemma compat_valid_from_option d : from_option Φ True d ⊢ ✓ d.
  Proof using HD.
    destruct d; cbn.
    - iIntros "H"; iPoseProof (compat_valid with "H") as "%H"; done.
    - iIntros "_". done.
  Qed.
  Lemma compat_update_from_option d1 d2 : d1 ~~> d2 → from_option Φ True d1 ⊢ |==> from_option Φ True d2.
  Proof using HD.
    destruct d1 as [d1|], d2 as [d2|].
    - intros H; cbn. apply compat_update. intros n [o|] Hno.
      + specialize (H n (Some (Some o))); apply H, Hno.
      + specialize (H n None); apply H, Hno.
    - by iIntros (_) "_ !>".
    - intros H; exfalso. specialize (H index_zero (Some (Some (DfracOwn 1)))).
      eapply dfrac_full_exclusive. rewrite cmra_comm. apply H.
      cbv -[Qp.le]. reflexivity.
    - by iIntros (_) "_ !>".
  Qed.
  Lemma compat_op_L_from_option d1 d2 : from_option Φ True d1 ∗ from_option Φ True d2 ⊢ from_option Φ True (d1 ⋅ d2).
  Proof using HD.
    destruct d1 as [d1|], d2 as [d2|]; cbn.
    - iIntros "H". by iApply compat_op.
    - iIntros "[H _]"; iApply "H".
    - iIntros "[_ H]"; iApply "H".
    - iIntros "_"; done.
  Qed.
  Lemma compat_op_R_from_option d1 d2 : from_option Φ True (d1 ⋅ d2) ⊢ from_option Φ True d1 ∗ from_option Φ True d2.
  Proof using HD.
    destruct d1 as [d1|], d2 as [d2|]; cbn.
    - iIntros "H". by iApply compat_op.
    - iIntros "H"; cbn; iFrame.
    - iIntros "H"; cbn; iFrame.
    - iIntros "_"; done.
  Qed.
End DFractionalFacts.

Section Instances.

Global Instance dfrac_dfractional `{SI: indexT} {Σ : gFunctors} `{!inG Σ dfracR} γ : DFractional (own γ).
Proof.
  split.
  - iIntros (d) "H". iPoseProof (own_valid with "H") as "%HH". done.
  - intros d1 d2; symmetry. apply own_op.
  - intros d Hd. apply _.
  - apply own_update.
Qed.

Global Instance gmap_view_frag_dfractional `{SI: indexT} {Σ : gFunctors} {K} {V} `{!EqDecision K} `{!Countable K}
      `{!inG Σ (gmap_viewUR K V)} γ k v : DFractional (λ dq, own γ (gmap_view_frag k dq v)). 
Proof.
  split.
  - iIntros (d) "H". iApply uPred.valid_entails; last by iApply own_valid.
    intros n Hn. by eapply gmap_view_frag_validN.
  - intros d1 d2; symmetry. rewrite gmap_view_frag_op. apply own_op.
  - intros d Hd. apply _.
  - intros d1 d2 Hupd. apply own_update, gmap_view_frag_dfrac, Hupd.
Qed.

Global Instance ghost_map_elem_dfractional `{SI: indexT} {Σ : gFunctors} {K} {V} `{!EqDecision K} `{!Countable K}
      `{!ghost_mapG Σ K V} γ k v : DFractional (λ dq, ghost_map_elem γ k dq v). 
Proof.
  rewrite ghost_map.ghost_map_elem_unseal. apply _.
Qed.

Global Instance mapsto_dfractional `{SI: indexT} {Σ : gFunctors} {K} {V} `{!EqDecision K} `{!Countable K}
      `{!gen_heapG K V Σ} k v : DFractional (λ dq, mapsto k dq v). 
Proof.
  rewrite mapsto_eq. apply _.
Qed.


End Instances.


Section QpUtils.


  Program Definition Qp_total_sub (m s : Qp) : (s<m)%Qp → Qp :=
    fun Hlt => match (Qp.sub m s) as o return (Qp.sub m s = o → Qp)
    with Some a => fun _ => a
       | None => fun H => _ end eq_refl.
  Next Obligation.
    intros m s Hlt Heq; exfalso; by apply Qp.sub_None,Qp.le_ngt in Heq.
  Qed.

  Lemma Qp_total_sub_spec_2 m s H r : Qp.sub m s = Some r → Qp_total_sub m s H = r.
  Proof.
    intros Heq2. unfold Qp_total_sub.
    eenough (forall ro (Heqq : (m-s)%Qp = ro),
      Qp_total_sub _ _ H = 
      match ro as o return (m-s)%Qp = o → Qp with
      | Some a => fun _ => a
      | None => fun HH => _ end Heqq) as H0
    by apply (H0 _ Heq2).
    intros ro Heqq. unfold Qp_total_sub. destruct Heqq. done.
  Qed.

  Lemma Qp_total_sub_spec m s H r : Qp_total_sub m s H = r → Qp.sub m s = Some r.
  Proof.
    intros Heq.
    destruct (m-s)%Qp eqn:Heq2 in |-*.
    - eapply Qp_total_sub_spec_2 in Heq2; erewrite Heq in Heq2; by rewrite Heq2.
    - by apply Qp.sub_None, Qp.le_ngt in Heq2.
  Qed.

  Lemma Qp_total_sub_ext m s H1 H2 : Qp_total_sub m s H1 = Qp_total_sub m s H2.
  Proof.
    destruct (m-s)%Qp eqn:Heq2 in |-; last first.
    - by apply Qp.sub_None, Qp.le_ngt in Heq2.
    - apply Qp_total_sub_spec_2, eq_sym; rewrite Heq2; f_equal; apply Qp_total_sub_spec_2; done.
  Qed.

  Lemma Qp_total_sub_lt_le m s H q : (m ≤ q)%Qp → (Qp_total_sub m s H < q)%Qp.
  Proof.
    pose proof (Qp_total_sub_spec _ _ H _ eq_refl) as Hm%Qp.sub_Some.
    rewrite {1}Hm. intros Htr. eapply Qp.lt_le_trans; last exact Htr.
    apply Qp.lt_add_r.
  Qed.

  Lemma Qp_total_sub_le_le m s H q : (m ≤ q)%Qp → (Qp_total_sub m s H ≤ q)%Qp.
  Proof. 
    intros HH. by apply Qp.lt_le_incl, Qp_total_sub_lt_le.
  Qed.

End QpUtils.

Section Store.
  Context `{SI: indexT} {Σ : gFunctors}.
  Context (A B : dfrac → iProp Σ).
  Context `{!DFractional A, !DFractional B}.

  Definition store_dfrac_inv (d1 d2 : option dfrac) := match d1, d2 with
  | Some (DfracOwn q), None => q = 1
  | None, Some (DfracOwn q') => q' = 1
  | Some (DfracOwn q), Some (DfracOwn q') => q + q' = 1
  | Some (DfracBoth true q), Some (DfracDiscarded) => q = 1
  | Some (DfracDiscarded), Some (DfracBoth true q') => q' = 1
  (* One side is always missing an infinitesimal, one side always has it *)
  | Some (DfracBoth true q), Some (DfracBoth false q') => q + q' = 1
  | Some (DfracBoth false q), Some (DfracBoth true q') => q + q' = 1
  | _, _ => False end%Qp.

  Lemma store_dfrac_inv_comm d1 d2 : store_dfrac_inv d1 d2 → store_dfrac_inv d2 d1.
  Proof. intros H. unfold store_dfrac_inv in *|-*; repeat case_match; simplify_eq; 
         cbn in *; try done; rewrite Qp.add_comm //. Qed.

  (* the option resource algebra does precisely what we want here.. *)
  Definition store_dfrac_can_trade (dt : dfrac) := ∀ d1 d2,
    ✓ (d1 ⋅ Some dt) ∧ ✓ d2 ∧ store_dfrac_inv d1 d2 →
    ∃ d1' d2', (d1 ⋅ Some dt) ~~> d1' ∧ d2 ~~> d2' ⋅ Some dt ∧ store_dfrac_inv d1' d2'.

  Lemma store_dfrac_can_trade_own t : store_dfrac_can_trade (DfracOwn t).
  Proof.
    intros [[q| |[] q]|] [[q'| |[] q']|] (H1&H2&H3); try done; repeat (unfold valid,cmra_valid,op,cmra_op in *|-*; cbn in *|-* ).
    - destruct (decide (q+t=1)%Qp).
      + eexists _, None. split; first done.
        assert (t = q') as -> by (rewrite -e in H3; by apply Qp.add_inj_r in H3).
        split; first done. apply H3.
      + assert (q+t<1)%Qp as Hlt by (by apply Qp.le_lteq in H1 as [H1|H1]; last done).
        rewrite -H3 in Hlt. apply Qp.add_lt_mono_l in Hlt.
        eexists _, (Some (DfracOwn (Qp_total_sub _ _ Hlt))). split; first done.
        pose proof (Qp_total_sub_spec _ _ Hlt _ eq_refl) as H%Qp.sub_Some. split_and!.
        * rewrite /= dfrac_op_own {1}H Qp.add_comm //.
        * cbn. rewrite -Qp.add_assoc -H H3 //.
    - exfalso. subst q. eapply Qp.le_ngt in H1. apply H1, Qp.lt_add_l.
    - subst q'.
      eexists _, (Some (DfracBoth true (Qp_total_sub _ _ H1))).
      pose proof (Qp_total_sub_spec _ _ H1 _ eq_refl) as Heq%Qp.sub_Some. split_and!.
      * done.
      * cbn. rewrite {1}Heq Qp.add_comm //.
      * cbn. done.
    - exfalso. subst q. eapply Qp.le_ngt in H1. apply H1, Qp.lt_add_l.
    - destruct (decide (q' = t)).
      + subst q'. eexists _, (Some (DfracDiscarded)).
        split_and!.
        * done.
        * done.
        * apply H3.
      + assert (t < q')%Qp as Hlt.
        { rewrite -H3 in H1. apply Qp.add_le_mono_l, Qp.le_lteq in H1 as [|]; done. }
        eexists _, (Some (DfracBoth false (Qp_total_sub _ _ Hlt))).
        pose proof (Qp_total_sub_spec _ _ Hlt _ eq_refl) as H%Qp.sub_Some. split_and!.
        * done.
        * cbn. rewrite cmra_comm {1}H //.
        * rewrite H in H3. rewrite Qp.add_assoc in H3. apply H3.
    - assert (t < q')%Qp as Hlt.
      { rewrite -H3 in H1; by eapply Qp.add_lt_mono_l. }
      eexists _, (Some (DfracBoth true (Qp_total_sub _ _ Hlt))).
      pose proof (Qp_total_sub_spec _ _ Hlt _ eq_refl) as H%Qp.sub_Some. split_and!.
      * done.
      * cbn. rewrite cmra_comm {1}H //.
      * rewrite H in H3. rewrite Qp.add_assoc in H3. apply H3.
    - subst q'. destruct (decide (t=1)%Qp).
      + subst t. eexists _, None. done.
      + apply Qp.le_lteq in H1 as [H1|H1]; last done.
        eexists _, (Some (DfracOwn (Qp_total_sub _ _ H1))).
        pose proof (Qp_total_sub_spec _ _ H1 _ eq_refl) as H%Qp.sub_Some. split_and!.
        * done.
        * rewrite /= {1}H cmra_comm //.
        * symmetry in H; done.
  Qed.

  Lemma store_dfrac_can_trade_discarded : store_dfrac_can_trade (DfracDiscarded).
  Proof.
    intros [[q| |[] q]|] [[q'| |[] q']|] (H1&H2&H3); try done; repeat (unfold valid,cmra_valid,op,cmra_op in *|-*; cbn in *|-* ).
    - eexists _, (Some (DfracBoth true q')). split_and!.
      * done.
      * apply option_update, dfrac_discard_update_infinitesimal.
      * done.
    - exfalso. subst q. by eapply Qp.lt_strict.
    - subst q'. eexists _, (Some (DfracBoth true 1)). done.
    - subst q. eexists _, (Some DfracDiscarded). done.
    - eexists _, (Some (DfracBoth false q')). done.
    - eexists _, (Some (DfracBoth true q')). done.
    - subst q'. eexists _, (Some (DfracBoth true 1)). split_and!.
      * done.
      * apply option_update, dfrac_discard_update_infinitesimal.
      * done.
  Qed.

  Lemma store_dfrac_can_trade_trans q1 q2 :
    store_dfrac_can_trade q1 → store_dfrac_can_trade q2 → store_dfrac_can_trade (q1 ⋅ q2).
  Proof.
    intros Ht1 Ht2 d1 d2 (HH1&HH2&HH3).
    rewrite Some_op cmra_assoc in HH1.
    edestruct (Ht1 d1 d2) as (d1'&d2'&HH1'&HH2'&HH3').
    { split_and; try done. eapply cmra_valid_op_l; done. }
    specialize (HH1' index_zero (Some _) HH1) as HH1s.
    specialize (HH2' index_zero None HH2) as HH2s.
    edestruct (Ht2 d1' d2') as (d1''&d2''&HH1''&HH2''&HH3'').
    { split_and!; try done. by eapply cmra_valid_op_l. }
    exists d1'', d2''. split_and!; try done.
    - etrans; last done. rewrite Some_op cmra_assoc. eapply cmra_update_op; done.
    - etrans; first done. rewrite (cmra_comm q1) Some_op cmra_assoc. eapply cmra_update_op; done.
  Qed.

  Lemma store_dfrac_can_trade_both_false q : store_dfrac_can_trade (DfracBoth false q).
  Proof.
    apply (store_dfrac_can_trade_trans (DfracOwn q) DfracDiscarded).
    - eapply store_dfrac_can_trade_own.
    - eapply store_dfrac_can_trade_discarded.
  Qed.

  Lemma store_dfrac_can_trade_both_true t : store_dfrac_can_trade (DfracBoth true t).
  Proof.
    intros [[q| |[] q]|] [[q'| |[] q']|] (H1&H2&H3); try done; repeat (unfold valid,cmra_valid,op,cmra_op in *|-*; cbn in *|-* ).
    - destruct (decide (t=q')).
      + subst t. eexists _, (Some (DfracDiscarded)). split_and!; try done.
        apply option_update, dfrac_discard_update_infinitesimal.
      + assert (t < q')%Qp as Hlt.
        { rewrite -H3 in H1. apply Qp.add_le_mono_l, Qp.le_lteq in H1 as [|]; done. }
        eexists _, (Some (DfracBoth false (Qp_total_sub _ _ Hlt))).
        pose proof (Qp_total_sub_spec _ _ Hlt _ eq_refl) as H%Qp.sub_Some. split_and!; try done.
        * cbn. rewrite {1}H cmra_comm. apply option_update, dfrac_discard_update_infinitesimal.
        * cbn. rewrite -Qp.add_assoc -H //.
    - exfalso. subst q. eapply Qp.le_ngt in H1. apply H1, Qp.lt_add_l.
    - subst q'. destruct (decide (t=1))%Qp.
      + subst t. eexists _, (Some DfracDiscarded). split; try done.
      + apply Qp.le_lteq in H1 as [H1|H1]; last done.
        eexists _, (Some (DfracBoth false (Qp_total_sub _ _ H1))).
        pose proof (Qp_total_sub_spec _ _ H1 _ eq_refl) as H%Qp.sub_Some. split_and!.
        * done.
        * rewrite /= {1}H cmra_comm //.
        * symmetry in H; done.
    - exfalso. subst q. eapply Qp.le_ngt in H1. apply H1, Qp.lt_add_l.
    - destruct (decide (t=q')).
      + subst t. eexists _, (Some (DfracDiscarded)). split_and!; try done.
        apply option_update, dfrac_discard_update_infinitesimal_again.
      + assert (t < q')%Qp as Hlt.
        { rewrite -H3 in H1. apply Qp.add_le_mono_l, Qp.le_lteq in H1 as [|]; done. }
        eexists _, (Some (DfracBoth false (Qp_total_sub _ _ Hlt))).
        pose proof (Qp_total_sub_spec _ _ Hlt _ eq_refl) as H%Qp.sub_Some. split_and!; try done.
        * cbn. rewrite {1}H cmra_comm. apply option_update, dfrac_discard_update_infinitesimal_again.
        * cbn. rewrite -Qp.add_assoc -H //.
    - destruct (decide (t=q')).
      + subst t. eexists _, (Some (DfracDiscarded)). split_and!; try done.
      + assert (t < q')%Qp as Hlt.
        { rewrite -H3 in H1. apply Qp.add_le_mono_l, Qp.le_lteq in H1 as [|]; done. }
        eexists _, (Some (DfracBoth false (Qp_total_sub _ _ Hlt))).
        pose proof (Qp_total_sub_spec _ _ Hlt _ eq_refl) as H%Qp.sub_Some. split_and!; try done.
        * cbn. rewrite {1}H cmra_comm. apply option_update, dfrac_discard_update_infinitesimal_again.
        * cbn. rewrite -Qp.add_assoc -H //.
    - subst q'. destruct (decide (t=1))%Qp.
      + subst t. eexists _, (Some DfracDiscarded). split_and!; try done.
        apply option_update, dfrac_discard_update_infinitesimal.
      + apply Qp.le_lteq in H1 as [H1|H1]; last done.
        eexists _, (Some (DfracBoth false (Qp_total_sub _ _ H1))).
        pose proof (Qp_total_sub_spec _ _ H1 _ eq_refl) as H%Qp.sub_Some. split_and!.
        * done.
        * rewrite /= {1}H cmra_comm. apply option_update, dfrac_discard_update_infinitesimal.
        * symmetry in H; done.
  Qed.

  Lemma store_dfrac_can_trade_always qp : store_dfrac_can_trade qp.
  Proof.
    destruct qp as [q| |[] q].
    - eapply store_dfrac_can_trade_own.
    - eapply store_dfrac_can_trade_discarded.
    - eapply store_dfrac_can_trade_both_true.
    - eapply store_dfrac_can_trade_both_false.
  Qed.

  Definition store_inv (d1 d2 : option dfrac) : iProp Σ
    := from_option A True%I d1 ∗ from_option B True%I d2 ∗ ⌜store_dfrac_inv d1 d2⌝.
  Definition store : iProp Σ := ∃ d1 d2, store_inv d1 d2.

  Lemma store_update_A_to_B dt : store ∗ A dt ⊢ |==> store ∗ B dt.
  Proof using All.
    iIntros "[(%d1&%d2&HHA&HHB&%HHdfrac) HA]".
    iPoseProof (compat_valid_from_option with "HHB") as "%HHB".
    iPoseProof (@compat_op_L_from_option _ _ A _ d1 (Some dt) with "[$]") as "HHA".
    iPoseProof (compat_valid_from_option with "HHA") as "%HHA".
    destruct (store_dfrac_can_trade_always dt d1 d2) as (d1'&d2'&HH1&HH2&HH3); first done.
    iPoseProof (compat_update_from_option _ _ HH1 with "HHA") as ">HHA".
    iPoseProof (compat_update_from_option _ _ HH2 with "HHB") as ">HHB".
    iPoseProof (compat_op_R_from_option with "HHB") as "[HHB1 $]".
    iModIntro. iExists d1', d2'. iFrame. done.
  Qed.

  Lemma store_update_B_to_A dt : store ∗ B dt ⊢ |==> store ∗ A dt.
  Proof using All.
    iIntros "[(%d1&%d2&HHA&HHB&%HHdfrac) HB]".
    iPoseProof (compat_valid_from_option with "HHA") as "%HHA".
    iPoseProof (@compat_op_L_from_option _ _ B _ d2 (Some dt) with "[$]") as "HHB".
    iPoseProof (compat_valid_from_option with "HHB") as "%HHB".
    apply store_dfrac_inv_comm in HHdfrac.
    destruct (store_dfrac_can_trade_always dt d2 d1) as (d2'&d1'&HH2&HH1&HH3); first done.
    iPoseProof (compat_update_from_option _ _ HH1 with "HHA") as ">HHA".
    iPoseProof (compat_update_from_option _ _ HH2 with "HHB") as ">HHB".
    iPoseProof (compat_op_R_from_option with "HHA") as "[HHA1 $]".
    apply store_dfrac_inv_comm in HH3.
    iModIntro. iExists d1', d2'. iFrame. done.
  Qed.

  Lemma store_init d1 d2 : from_option A True d1 ∗ from_option B True d2 ∗ ⌜store_dfrac_inv d1 d2⌝ ⊢ store.
  Proof.
    iIntros "(H1&H2&H3)".
    iExists d1, d2; iFrame.
  Qed.

  Lemma store_init_A : A (DfracOwn 1) ⊢ store.
  Proof.
    iIntros "HA".
    iApply (store_init (Some (DfracOwn 1)) None). by iFrame.
  Qed.

  Lemma store_init_B : B (DfracOwn 1) ⊢ store.
  Proof.
    iIntros "HB".
    iApply (store_init None (Some (DfracOwn 1))). by iFrame.
  Qed.

End Store.


